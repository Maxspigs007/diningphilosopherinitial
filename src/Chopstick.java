public class Chopstick {
  private int ID;
   boolean libre;

  Chopstick(int ID) {
    this.ID = ID;
    libre = true;
  }

  synchronized void take() {
   while(!libre) {
      try {
        wait();
      } catch(InterruptedException e) {
        System.out.println("boom !");
      }
    }
    libre = false;

  }

  synchronized void release() {
  notifyAll();
    libre = true;

  }
    
  public int getID() {
    return(ID);
  }
}
