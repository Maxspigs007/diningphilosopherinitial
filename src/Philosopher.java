public class Philosopher extends Thread {
  private GraphicTable table;
  private Chopstick left;
  private Chopstick right;
  private int ID;
  final int timeThink_max = 5000;
  final int timeNextFork = 100;
  final int timeEat_max = 2000;

  Philosopher(int ID, GraphicTable table, Chopstick left, Chopstick right) {
    this.ID = ID;
    this.table = table;
    this.left = left;
    this.right = right;
    setName("Philosopher "+ID);
  }

public void run() {
    for(;;) {

    	table.isThinking(ID);
        System.out.println(getName()+" thinks");
        try {
        	sleep((long)(Math.random()*timeThink_max));
        	} catch(InterruptedException e) {
        	System.out.println(e);
        }        
        System.out.println(getName()+" finished thinking");               
 
    System.out.println(getName()+" is hungry");               
      table.isHungry(ID);

      System.out.println(getName()+" wants left chopstick");
      left.take();
      table.takeChopstick(ID, left.getID());
      System.out.println(getName()+" got left chopstick");
      
      try {
	sleep(timeNextFork);
      } catch(InterruptedException e) {
	System.out.println(e);
      }     
      
      System.out.println(getName()+" wants right chopstick");
      right.take();
      table.takeChopstick(ID, right.getID());
      System.out.println(getName()+" got right chopstick");

      System.out.println(getName()+" eats");               
      try {
	sleep((long)(Math.random()*timeEat_max));
      } catch(InterruptedException e) {
	System.out.println(e);
      }

      System.out.println(getName()+" finished eating");               
      
      table.releaseChopstick(ID, left.getID());
      left.release();
      System.out.println(getName()+" released left chopstick");
     
      table.releaseChopstick(ID, right.getID());
      right.release();
      System.out.println(getName()+" released right chopstick");

     }
  }
}
